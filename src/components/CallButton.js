import React from 'react';

export default class CallButton extends React.Component {
    acessaTopo() {
        window.scrollTo(0, 0);
    }

    render() {
        return (
            <div className="callTop" onClick={this.acessaTopo}></div>
        )
    }
}