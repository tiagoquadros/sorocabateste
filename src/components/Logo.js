import React from 'react';

export default class Logo extends React.Component {
  render() {
    return (
      <header>
          <div className="logo">
            <img src='assets/logo.png' alt="SUPERGIANTGAMES" />
            <h1>SUPERGIANTGAMES</h1>
          </div>
      </header>
    )
  }
}