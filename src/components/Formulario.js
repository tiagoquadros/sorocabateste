import React from 'react';

export default class Formulario extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            display: 'none',
            text: '',
            classe: ''
        }

        this.validaEnvio = this.validaEnvio.bind(this);
        this.retiraAlert = this.retiraAlert.bind(this);
    }

    validaEnvio(e) {
        e.preventDefault();
        let inputs = document.getElementsByClassName("campo");
        let falha = false;
        Array.prototype.forEach.call(inputs, function(valor) {
            if(valor.value.trim().length == 0) {
                falha = true;
            }
        });
        this.setState({classe: ((falha)?'error':'success')});
        this.setState({text: ((falha)?'Deu tudo errado! Clique em mim para fechar.':'Deu tudo certo! Clique em mim para fechar. (Banca o malandrão e remove um required kk)')});
        this.setState({display: 'block'});
    }    

    retiraAlert() {
        this.setState({display: 'none'});
    }

    render() {
        return (
            <form method="GET" onSubmit={this.validaEnvio}>
                <h2>FORMULÁRIO</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                <div className={'alert '+this.state.classe} style={this.state} onClick={this.retiraAlert}>
                    {this.state.text}
                </div>
                <div className="inline-inputs">
                    <input type="text" className="campo" name="nome" placeholder="Nome"  required />
                    <input type="text" className="campo" name="email" placeholder="Email" required />
                </div>
                <div className="inline-inputs">
                    <textarea name="mensagem" className="campo" placeholder="Mensagem" required ></textarea>
                </div>
                <button type="submit">ENVIAR</button>
            </form>
        );
    }
}