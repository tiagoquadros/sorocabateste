import React from 'react';

export default class Card extends React.Component {
    render() {
        return (
            <div className="card">
                <div className="char-profile">
                    <img src={this.props.char.image} alt="Jogo" />
                </div>
                <p className="char-description">
                    {this.props.char.descricao}
                </p>
            </div>
        );
    }
}
