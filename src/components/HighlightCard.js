import React from 'react';

export default class HighlightCard extends React.Component {
    render() {
        return (
        <div className="char-highlight">
            <div className="char-title">
                TRANSISTOR - RED THE SINGER
            </div>
            <div className="char-profile">
                <img src="assets/card-highlight.png" width="90%" alt="TRANSISTOR - RED THE SINGER" />
            </div>
            <div className="char-description">
                "Olha, o que quer que você esteja pensando, me faça um favor, não solte."
            </div>
        </div>
        );
    }
}
