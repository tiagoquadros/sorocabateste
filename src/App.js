import React from 'react';
import './App.css';
import Logo from './components/Logo';
import Card from './components/Card';
import Formulario from './components/Formulario';
import HighlightCard from './components/HighlightCard';
import CallButton from './components/CallButton';
import Controls from './components/Controls';

const chars = {
  grant: {
    descricao: 'A Camerata foi apenas os dois no início, e suas fileiras nunca foram destinadas a exceder um número a ser contado em uma mão.',
    image: 'assets/Grant.png'
  },
  red: {
    descricao: 'Red, uma jovem cantora, entrou em posse do Transistor. Sendo a poderosa espada falante. O grupo Possessores quer tanto ela quanto o Transistor e está perseguindo implacavelmente a sua procura.',
    image: 'assets/Red.png'
  },
  sybil: {
    descricao: 'Sybil é descrita pelo Transistor como sendo os "olhos e ouvidos" da Camerata.',
    image: 'assets/Sybil_2.png'
  }
}

export default class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='site' id="topo">
        e tá doido.
        <Logo />
        <section className='start' style={{backgroundImage: "url(" + "assets/main-bg.png" + ")"}}>
          <HighlightCard />
        </section>
        <section className='characters'>
          <Card char={chars.grant} />
          <Card char={chars.red} />
          <Card char={chars.sybil} />
          <Controls />
        </section>
        <section className='form'>
          <Formulario />
        </section>
        <footer>
        </footer>
        <CallButton onClick={'location.hash='+this.topo} />
      </div>
    );
  }
}
